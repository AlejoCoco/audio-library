<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Guest extends CI_Controller {
    public function __construct() {
        parent::__construct();
     $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('cookie');
        $this->load->model('Models');
        
    }
    function index() {
        $this->load->view('includes/header');
        $this->load->view('Guest');   
         $this->load->view('includes/footer');    
    }
}
