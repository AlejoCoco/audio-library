<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Assets extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
            $seconds_to_cache = 7200;
        $ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
        header("Expires: $ts");
        header("Pragma: cache");
        header("Cache-Control: max-age=$seconds_to_cache");
    }

    function js($e) {
        if (file_exists('js/' . $this->uri->segment(3))) {
            header('Content-type: text/javascript');
            function sanitize_output($buffer) {
//                $search = array('#\s*\/\/.*$#m', '#\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#', '#[;,]([\]\}])#', '#\btrue\b#', '#\bfalse\b#', '#\breturn\s+#');
//                $replace = array("", '$1', '$1', '!0', '!1', 'return ');
//                $buffer = preg_replace($search, $replace, $buffer);

                $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
                $buffer = str_replace(["\r\n","\r","\n","\t",'  ','    ','     '], '', $buffer);
                $buffer = preg_replace(['(( )+{)','({( )+)'], '{', $buffer);
                $buffer = preg_replace(['(( )+})','(}( )+)','(;( )*})'], '}', $buffer);
                $buffer = preg_replace(['(;( )+)','(( )+;)'], ';', $buffer);
                
                return $buffer;
            }

            ob_start("sanitize_output");
            include_once 'js/' . $this->uri->segment(3);
        } else {
            redirect(site_url());
        }
    }

    function css() {
        if (file_exists('css/' . $this->uri->segment(3))) {
            header("Content-type: text/css; charset: UTF-8");
            function sanitize_css($code) {
                $code = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $code);
                $code = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $code);
                $code = str_replace('{ ', '{', $code);
                $code = str_replace(' }', '}', $code);
                $code = str_replace('; ', ';', $code);
                return $code;
            }
            ob_start("sanitize_css");
            include_once 'css/' . $this->uri->segment(3);
        } else {
            redirect(site_url());
        }
    }

    function index() {
        redirect(site_url());
    }

}
