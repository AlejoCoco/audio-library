<?php
if((!$this->uri->segment(3)) or (!$this->uri->segment(4))){
    redirect(site_url('transfer_list'));
}
?>
<div class="content">
   <div class="container-fluid">
      <div class="create-transfer-form animated fadeIn widget grid6" method="post">
         <div class="panel-heading">
            <h3>
               From
               <?php
               echo date('M/d/Y', strtotime(str_replace('-', '/', $this->uri->segment(3))));
               ?>
               To
               <?php
               echo date('M/d/Y', strtotime(str_replace('-', '/', $this->uri->segment(4))));
               ?>
            </h3> 
         </div>
         <div class="panel-body overflow-x">


            <table class="inbox-table table table-bordered">
               <thead>
                  <tr>
                     <th class="hide-xs text-center">STATUS</th>
                     <th class="hide-xs description">CLIENT NAME</th>
                     <th class="hide-xs text-center">TRANSFER FROM</th>
                     <th class="ellipsis" style="word-wrap: break-word;" data-original-title="" title="">TRANSFER TO</th>
                     <th class="hide-xs text-center">TRANSFER START</th>
                     <th class="hide-xs text-center">TRANSFER END</th>
                     <th class="hide-xs text-center">DURATION</th>
                     <th class="hide-xs text-center">SENDER</th>
                     <th class="hide-xs text-center"># OF IT</th>
                     <th class="hide-xs text-center"># OF PC</th>
                  </tr>
               </thead>
               <tbody class="">
                   <?php
                   $query = $this->Models->transfer_all_date();
                   $row = $query->result();
                   $array = array();
                   $total = 0;
                   $all = 0;
                   foreach ($row as $value) {
                       ?>
                      <tr>
                         <td class="text-center"><a href="<?php echo base_url('transfer/issue_id/' . $value->IDT) ?>"><?php echo ($value->client_support == 1 ? 'SUPPORT' : 'CLIENT') ?></a></td>
                         <td class="text-center"><?php echo $value->client_name ?></td>
                         <td class="text-center"><?php echo $value->transfer_from ?></td>
                         <td class="text-center"><?php echo $value->transfer_to ?></td>
                         <td class="text-center"><?php echo ($value->transfer_start == '' ? '' : date('m/d/Y H:i:s', $value->transfer_start)) ?></td>
                         <td class="text-center"><?php echo ($value->transfer_end == '' ? '' : date('m/d/Y H:i:s', $value->transfer_end)) ?></td>
                         <td class="text-center">
                             <?php
                             echo $total = ($value->transfer_end === '' ? '00:00:00' : date_create(date('m/d/Y h:i:s A', $value->transfer_start))->diff(date_create(date('m/d/Y h:i:s A', $value->transfer_end)))->format('%D:%H:%I'));
                             $array[] = $total;
                             ?>
                         </td>
                         <td class="text-center">
                             <?php
                             $_user = $this->Models->user_id($value->transfer_involve);
                             $_userid = $_user->result();
                             echo $_userid[0]->fname . ' ' . $_userid[0]->lastname
                             ?>
                         </td>
                         <td class="text-center">
                             <?php
                             $allit = $this->Models->transfer_assignee($value->IDT);
                             echo $allit->num_rows();
                             ?>

                         </td>
                         <td class="text-center">
                             <?php
                             $allpc = $this->Models->show_add_pc($value->IDT);
                             echo $total = $allpc->num_rows();
                             $all += $total;
                             ?>
                         </td>
                      </tr>
                      <?php
                  }
                  ?>

                  <tr> 
                     <td class="text-center" colspan="6"></td> 

                     <td class="text-center"> 
                         <?php
                         echo $this->Models->AddPlayTime($array);
                         ?>

                     </td> 

                     <td class="text-center" colspan="2"> </td> 
                     <td class="text-center">
                         <?php echo $all ?>
                     </td> 
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <div class="footer text-center muted">Ticketing System The Outsourced Accountant 2017</div>
</div>
<script>
    (function (j) {
        j(window).load(function () {

        });
    })(jQuery);
</script>  