(function (j) {
    j.message = function (options, callback) {
        var _body = j('body');
        var settings = j.extend({
            title: "Message",
            text: "Add some text",
            button: "Proceed"
        }, options);
        var _html = j('<div>');
        _html.addClass('message-dialog');
        if (_body.find('.message-dialog').length === 0) {
            _body.append(_html);
        }
        _body.find('.message-dialog').html(settings.text);
        j('.message-dialog').dialog({
            title: settings.title,
            modal: false,
            draggable: false,
            buttons: [
                {
                    text: settings.button,
                    click: function () {
                        j(this).dialog("close");
                        callback.call(this);
                    }
                },{
                    text: "OK",
                    click: function () {
                        j(this).dialog("close");
                    }
                }

            ]
        });
    };
})(jQuery);

(function (j) {
    j.alert = function (options) {
        var _body = j('body');
        var settings = j.extend({
            title: "Message",
            text: "Add some text",
            button: "Proceed"
        }, options);
        var _html = j('<div>');
        _html.addClass('alert-dialog');
        if (_body.find('.alert-dialog').length === 0) {
            _body.append(_html);
        }
        _body.find('.alert-dialog').html(settings.text);
        j('.alert-dialog').dialog({
            title: settings.title,
            modal: true,
            buttons: [
               {
                    text: "OK",
                    click: function () {
                        j(this).dialog("close");
                    }
                }

            ]
        });
    };
})(jQuery);


