<?php
include_once ("library.php");
$func = new Library();

?>

    <div class="container" style="padding-top:35px;">
        <div class="row" style="background-color: #446CB3;">
            <div class="col-sm-12" style="padding:12px;">
                <img src="images/logo-light.svg" height="80px;">
                <br>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Audio Category</h3></div>
                    <div class="panel-body" >
                        <div class="panel panel-default addcategorylist">
                             <div id="chatlist" class="mousescroll">
   
                            <?php        
                                $getlastid = "SELECT * FROM category_tbl";
                                $execute_lastid = $conn->query($getlastid);
                                //while($row_validation_cat = $execute_lastid->fetch_assoc()) {
                               // $validation_cat = $row_validation_cat['category_name'];
                                //$validation_cat_id = $row_validation_cat['ID'];
                            ?>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="1" ><i class="far fa-play-circle">&nbsp;1. Allan Mason - Five steps to determine the right structure for your business.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="2" ><i class="far fa-play-circle">&nbsp;2. Amanda Dyason - 7 Key Numbers You Need to Inc…Your Profits.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="3" ><i class="far fa-play-circle">&nbsp;3. Amber Worth-Syncing Your Laptop, Phone and Tab…g Office 365.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                             <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="4" ><i class="far fa-play-circle">&nbsp;4. Andrew Cominos - The 5 Steps to Successful Sales.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                             <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="5" ><i class="far fa-play-circle">&nbsp;5. Andy Tunks - Creating Engaging Facebook advertise…ving money.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="6" ><i class="far fa-play-circle">&nbsp;6. Andy Tunks - Using Facebook advertising to generate saless.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                             <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="7" ><i class="far fa-play-circle">&nbsp;7. Dave Meikle - 5 Strategies to Use Video in Your Sal…ngagement.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                             <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="8" ><i class="far fa-play-circle">&nbsp;8. Dave Meikle - How to avoid getting overlooked on social media.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="9" ><i class="far fa-play-circle">&nbsp;9. David Pitman - Seven Steps to scalable Integrated …anagement.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="10" ><i class="far fa-play-circle">&nbsp;10. David Pitman - Choosing the Right CRM for Your Business.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="11" ><i class="far fa-play-circle">&nbsp;11. Debbie Kropp - 5 Steps to Get Your Website Working for You.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="12" ><i class="far fa-play-circle">&nbsp;12. Justin Dunkley - The First Things to do to Kickstart …r Marketing.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="13" ><i class="far fa-play-circle">&nbsp;13. Kevin Gammie - 4 Ways We Procrastinate and how to stop it.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                              <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="14" ><i class="far fa-play-circle">&nbsp;14. Kevin Gammie - Finding Yor Business Confidence.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                             <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="15" ><i class="far fa-play-circle">&nbsp;15. Kevin Gammie - How to build a Workshop.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="16" ><i class="far fa-play-circle">&nbsp;16. Kevin Gammie - The Marketing Plan.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="17" ><i class="far fa-play-circle">&nbsp;17. Kevin Gammie - The Vision.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="18" ><i class="far fa-play-circle">&nbsp;18. Kevin Gammie - Building a sales Funnel to Stop Was…ocial Media.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="19" ><i class="far fa-play-circle">&nbsp;19. Lynne Spalding - Turning Profits into Cash.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="20" ><i class="far fa-play-circle">&nbsp;20. Maria Anderson - 5 Steps to Creating Content that Sells.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="20" ><i class="far fa-play-circle">&nbsp;20. Maria Anderson - 5 Steps to Creating Content that Sells.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="21" ><i class="far fa-play-circle">&nbsp;21. Melissa Stanford - 8 Steps to successfully employ a…r Workplace.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="22" ><i class="far fa-play-circle">&nbsp;22. Neil Daramow - How to write recruitment ads that…ight people.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="23" ><i class="far fa-play-circle">&nbsp;23. Sandra Price - From Shoebox to organised - Gettin…unts sorted.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="24" ><i class="far fa-play-circle">&nbsp;24. Sandra Price - The Six Deadly Bookkeeping sins cos…u a fortune.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="25" ><i class="far fa-play-circle">&nbsp;25. Tom Freer - Protect Your Business from Cyber Threats.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            <div class="panel-body panel-side del<?php echo $validation_cat_id; ?>"> 
                                <p class="all-<?php echo $validation_cat_id; ?>">
                                    <a class="audio-play" audio-id="26" ><i class="far fa-play-circle">&nbsp;26. Vicki Parker - How to run a professional MLM with…our friends.mp3</i> </a><?php //echo  $validation_cat; ?>
                                </p>

                            </div>
                            


                            <?php
                              //  }
                            ?>
                        </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-sm-7">

                <div class="row">
                    <div class="col-sm-12 ">
                        <div class="panel panel-default audio-banner">
                            <img src="images/mic.png" class="img-profile">
                            <h1 class="play_caption"><?php //echo strtoupper("Audio.mp3"); ?></h1>
                            <h5 class="play_caption_filename"><?php //echo strtoupper("Stacey Sy"); ?></h5>
                            <audio id="blast" class="filepath" src="http://stream.arrowcaz.nl/caz64kmp3" controls "loop" >

                            </audio>
                        </div>
                    </div>
                </div>
               <div class="row">
                    <div class="col-sm-12 ">
                          <div class="panel panel-default audio-banner">
                         
                                
                                   
                                    <div class="input-group"> <span class="input-group-addon" id="basic-addon3">Copy the link</span> 
                                     <input type="text" class="copy_path form-control" style="border: 1px solid #e8e6e4!important;" name="copy_path" value="">
                                      </div>
                                
                    </div>
                     </div>
                </div>
               
                   

             
            </div>
            <div class="col-sm-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fas fa-cog"></i> Settings</div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-body panel-side hide"> <a class="category_btn">Category</a></div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body panel-side" style="text-align: center;"> 
                           <h6>@2018 BSB Audio Library all rights reserved.</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="category" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Category</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                        <form class="form-category" method="post" enctype="multipart/form-data">
                            <div class="col-sm-3">
                                <strong>Category Name</strong>
                            </div>
                            <div class="col-sm-7" style="text-align: left;">
                                <input type="text" class="form-control categoryname" name="categoryname" placeholder="Enter category name" style="border: 1px solid gray;">
                            </div>
                            <div class="col-sm-2" style="padding-bottom: 10px;">
                                    
                            </div>
                            <div class="col-sm-3">
                                <strong>File Upload</strong>
                            </div>
                            <div class="col-sm-7" style="text-align: left;">
                               <div class="input-group"> 
                             
                               <input type="text" class="form-control fake-path" placeholder="doc,ppt,xsl,pdf,image only" readonly="readonly" aria-invalid="false"> <span class="input-group-btn"> <button class="btn btn-default attached" type="button">Attach file <i class="fa fa-paperclip" aria-hidden="true"></i> MAX 5MB</button> 
                           </div>
                            </div>
                            <div class="col-sm-2" style="padding-bottom: 10px;">
                                   <input type="file" name="file[]" class="form-control fileupload" multiple="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword, application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*">
                                     
                            </div>

                            
                             <div class="col-sm-2">
                                <button type="submit" class="btn btn-success addcategory">Add</button>
                            </div>
                        </form>
                        
                        <form class="form-category-search" method="post">
                            <div class="col-sm-3" style="padding: 10px;">
                                <strong>Search Category</strong>
                            </div>
                            <div class="col-sm-7" style="text-align: left;">
                                <input type="text" class="form-control filtercategory" name="filtercategory" placeholder="Type category name" style="border: 1px solid gray;">
                            </div>
                            <div class="col-sm-2">
                               
                            </div>
                        </form>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    
                                    <table class="table" width="100%">
                                        <thead >
                                            <tr>
                                                <th>#ID</th>
                                                <th>Category Name</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody  class="searchablecategory" >
                                        <?php
                                          //  $sql = "SELECT * FROM category_tbl";
                                           // $result = $conn->query($sql);
                                          //  while($row = $result->fetch_assoc()) {
                                        ?>
                                        <form  class="form_info" >
                                            <tr class= "<?php echo  $row["ID"]; ?>">
                                                <td width="33.35%" ><?php echo  $row["ID"]; ?></td>
                                                <td width="33.35%"><input type="text" class="category_field form-control" value="<?php echo  $row["category_name"]; ?>" name="category_name"></td>
                                                <td width="33.35%">
                                                    <input type="hidden" name="id_cat" value="<?php echo  $row["ID"]; ?>">
                                                     <button type="submit" cat-id="<?php echo  $row["ID"]; ?>" class="btn btn-default btn-xs categorytd_update_btn"><i class="fas fa-pen-square"></i>Update</button>

                                                      <button type="submit" cat-del-id="<?php echo  $row["ID"]; ?>" class="btn btn-danger btn-xs categorytd_delete_btn"><i class="fas fa-trash"></i>Delete</button>
                                                  
                                                </td>

                                            </tr>
                                         </form>
                                        <?php
                                           // }
                                        ?>
                                        
                                        </tbody>
                                    </table>
                                    <table class="table" width="100%">
                                        <tbody class="tableappend searchablecategory">
                                          <form class="tableappend form_info_append" novalidate="novalidate">

                                          </form>
                                        </tbody>
                                    </table>
                               
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

   <script>
          (function (j) {
              j(function () {
                  EM.script.init();
              });
          })(jQuery);
      </script>   