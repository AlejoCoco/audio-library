<?php
include_once ("library.php");
$func = new Library();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Audio Library</title>
  <meta charset="utf-8">
  <meta name="site" content="<?php echo $func->base_url(); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
 
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <link rel="stylesheet" href="<?php echo $func->base_url().'css/style.css'; ?>">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link href="<?php echo $func->base_url().'jquery-ui/jquery-ui.min.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $func->base_url().'jquery-ui/jquery-ui.structure.min.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $func->base_url().'jquery-ui/jquery-ui.theme.min.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $func->base_url().'css/animate.min.css'; ?>" rel="stylesheet">
        <link href="<?php echo $func->base_url().'css/theme.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $func->base_url().'css/font.min.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $func->base_url().'css/datatables.bootstrap.min.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $func->base_url().'css/template.css?ver=5'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $func->base_url().'css/pnotify.css'; ?>" rel="stylesheet">
        <link href="<?php echo $func->base_url().'css/pnotify.buttons.css'; ?>" rel="stylesheet">
        <link href="<?php echo $func->base_url().'css/pnotify.nonblock.css'; ?>" rel="stylesheet">
        <link href="<?php echo $func->base_url().'css/fullcalendar.min.css'; ?>" rel='stylesheet' />
        <link href="<?php echo $func->base_url().'css/fullcalendar.print.css'; ?>" rel='stylesheet' media='print' />
        <link href="<?php echo $func->base_url().'css/jquery.datetimepicker.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $func->base_url().'css/jquery.qtip.min.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $func->base_url().'css/stylesheet.css'; ?>" rel="stylesheet" type="text/css"/>


  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="<?php echo $func->base_url().'js/script.js'; ?>"></script>
    <script src="<?php echo $func->base_url().'js/jquery.validate.min.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/jquery.ellipsis.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/notify.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/jquery.timepicker.min.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/jquery.cropit.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/confirm.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/message.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/cookies.js'; ?>" type="text/javascript"></script>
        <script src='<?php echo $func->base_url().'js/jquery.datetimepicker.full.min.js'; ?>'></script>
        <script src="<?php echo $func->base_url().'js/pubnub.4.0.11.min.js'; ?>" type="text/javascript"></script>
        <script src="<?php echo $func->base_url().'js/pnotify.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/pnotify.buttons.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/pnotify.nonblock.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/pnotify.desktop.js'; ?>"></script>
        <script src='<?php echo $func->base_url().'js/moment.min.js'; ?>'></script>
        <script src='<?php echo $func->base_url().'js/jquery.qtip.min.js'; ?>'></script>
        <script src='<?php echo $func->base_url().'js/fullcalendar.min.js'; ?>'></script>
        <script src="<?php echo $func->base_url().'js/Chart.bundle.min.js'; ?>" type="text/javascript"></script>
                <script src="<?php echo $func->base_url().'js/pace.min.js'; ?>" data-pace-options='{ "ajax": false }' ></script>
      
        <script src="<?php echo $func->base_url().'js/jquery.datatables.min.js'; ?>" type="text/javascript"></script>
        <script src="<?php echo $func->base_url().'js/datatables.bootstrap.min.js'; ?>" type="text/javascript"></script>
        <script src="<?php echo $func->base_url().'js/bootstrap.min.js'; ?>" type="text/javascript"></script>
        <script src="<?php echo $func->base_url().'jquery-ui/jquery-ui.min.js'; ?>"></script>
        <script src="<?php echo $func->base_url().'js/jquery.form.min.js'; ?>" type="text/javascript"></script>
</head>
 
