var EM = EM || {};
EM.script = (function (j) {
    var _url = j('[name="site"]').attr('content');
    var init = function () {
       
        consolelog();
        categoryModal();
        add_category();
        categoryfilter();
        update_category_record();
        delete_category();
        fileattached();
        play_audio();
        scroll();
        scrolldiv();
    };

    function consolelog(){
        console.log("Initialize");
    }
    function categoryfilter(){
        $('.filtercategory').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchablecategory tr').hide();
            $('.searchablecategory tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })
    }
    function scrolldiv(){
         j('#chatlist').slimscroll({
                  color: '#00f',
                  size: '10px',
                  width: '100%',
                  height: '300px'                  
              });
    
    }



    function categoryModal(){
        var _adduser = j(".category_btn");
        var modalbox = j("#category");
        _adduser.click(function () {
            modalbox.modal({backdrop: 'static', keyboard: false});
        });
        modalbox.find('.close').click('click', function (e) {
            e.preventDefault();
            modalbox.modal('hide');
        });
    }
    var delete_category = function () {

  j('.categorytd_delete_btn').each(function(){

        j(this).unbind().bind('click', function (e) {
          
            e.preventDefault();
            var _this = j(this);
           console.log(_this.attr('cat-del-id'));
          

                j.ajax({
                    url: _url + 'postdata',
                    dataType: 'json',
                    method: 'post',
                    data: {"get_delete_id": _this.attr('cat-del-id')},
                    success: function (e) {
                        if (e.message === 'success') {
                                j.notify(e);
                                console.log(e.message);
                                j("."+_this.attr('cat-del-id')).addClass("hide");
                                 j(".del"+_this.attr('cat-del-id')).addClass("hide");
                            
                        }
                    }
                });
         });


        });
    };
var update_category_record_append = function () {
         var _form = j('.form_info');

        _form.each(function () {
            var _this = j(this);
            _this.validate({
                rules: {
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.parent().addClass('has-error');
                        _elm.popover({content: value.message, trigger: 'focus', placement: 'top'});
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.parent().removeClass('has-error');
                        _elm.popover('destroy');
                    });
                },
                submitHandler: function (form) {
                    var _form = j(form);
                    console.log(_form.serialize());
                    j.ajax({
                        url: _url + 'postdata',
                        dataType: 'json',
                        method: 'post',
                        data: _form.serialize(),
                         success: function (output) {
                            if (output.message === 'success') {
                                j.notify(output);
                                console.log("this is "+output.id);
                                j(".all-"+output.id).html("<i class='far fa-play-circle'></i> "+output.catname);
                            }

                            /*
                             if (output.message === 'success') {
                             j.notify(output);
                             console.log('aal');
                             setTimeout(function () {
                             
                             }, 4000);
                             }
                             console.log(output);
                             // _form.find('.output').html(output.text);
                             */
                        }
                    });
                    return false;
                }
            });
        });
    };

    var update_category_record = function () {
         var _form = j('.form_info');

        _form.each(function () {
            var _this = j(this);
            _this.validate({
                rules: {
                },
                showErrors: function (errorMap, errorList) {
                    j.each(this.errorList, function (index, value) {
                        var _elm = j(value.element);
                        _elm.parent().addClass('has-error');
                        _elm.popover({content: value.message, trigger: 'focus', placement: 'top'});
                    });
                    j.each(this.successList, function (index, value) {
                        var _elm = j(value);
                        _elm.parent().removeClass('has-error');
                        _elm.popover('destroy');
                    });
                },
                submitHandler: function (form) {
                    var _form = j(form);
                    console.log(_form.serialize());
                    j.ajax({
                        url: _url + 'postdata',
                        dataType: 'json',
                        method: 'post',
                        data: _form.serialize(),
                         success: function (output) {
                            if (output.message === 'success') {
                                j.notify(output);
                                console.log("this is "+output.id);
                                j(".all-"+output.id).html("<i class='far fa-play-circle'></i> "+output.catname);
                                j(".addcategorylist").append();
                            }

                            /*
                             if (output.message === 'success') {
                             j.notify(output);
                             console.log('aal');
                             setTimeout(function () {
                             
                             }, 4000);
                             }
                             console.log(output);
                             // _form.find('.output').html(output.text);
                             */
                        }
                    });
                    return false;
                }
            });
        });
    };


    var play_audio = function (){
        var play_audio = j(".audio-play");

         j(play_audio).unbind().bind('click', function (e) {
                   var ID = j(this).attr("audio-id");
                   console.log(ID);
                   j('.filepath').attr('src', _url + "uploads/"+ID+".mp3");

                   

                   if(ID == 1){
                        j('.play_caption').html("Allan Mason");
                        j('.play_caption_filename').html("1. Five steps to determine the right structure for your business.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 2){
                        j('.play_caption').html("Amanda Dyason ");
                        j('.play_caption_filename').html("2. 7 Key Numbers You Need to Inc…Your Profits.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 3){
                        j('.play_caption').html("Amber Worth ");
                        j('.play_caption_filename').html("3. Syncing Your Laptop, Phone and Tab…g Office 365.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 4){
                        j('.play_caption').html("Andrew Cominos  ");
                        j('.play_caption_filename').html("4. The 5 Steps to Successful Sales.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 5){
                        j('.play_caption').html("Andy Tunks  ");
                        j('.play_caption_filename').html("5. Creating Engaging Facebook advertise…ving money.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 6){
                        j('.play_caption').html("Andy Tunks  ");
                        j('.play_caption_filename').html("6.  Using Facebook advertising to generate saless.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 7){
                        j('.play_caption').html("Dave Meikle  ");
                        j('.play_caption_filename').html("7. 5 Strategies to Use Video in Your Sal…ngagement.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 8){
                        j('.play_caption').html("Dave Meikle  ");
                        j('.play_caption_filename').html("8. How to avoid getting overlooked on social media.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 9){
                        j('.play_caption').html("David Pitman  ");
                        j('.play_caption_filename').html("9. Seven Steps to scalable Integrated …anagement.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 10){
                        j('.play_caption').html("David Pitman  ");
                        j('.play_caption_filename').html("10. Choosing the Right CRM for Your Business.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 11){
                        j('.play_caption').html("Debbie Kropp  ");
                        j('.play_caption_filename').html("11. 5 Steps to Get Your Website Working for You.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 12){
                        j('.play_caption').html("Justin Dunkley  ");
                        j('.play_caption_filename').html("12.  The First Things to do to Kickstart …r Marketing.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 13){
                        j('.play_caption').html("Kevin Gammie   ");
                        j('.play_caption_filename').html("13.  4 Ways We Procrastinate and how to stop it.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 14){
                        j('.play_caption').html("Kevin Gammie   ");
                        j('.play_caption_filename').html("14.  Finding Yor Business Confidence.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 15){
                        j('.play_caption').html("Kevin Gammie   ");
                        j('.play_caption_filename').html("15.  How to build a Workshop.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 16){
                        j('.play_caption').html("Kevin Gammie   ");
                        j('.play_caption_filename').html("16.  The Marketing Plan.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 17){
                        j('.play_caption').html("Kevin Gammie   ");
                        j('.play_caption_filename').html("17.  The Vision.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 18){
                        j('.play_caption').html("Kevin Gammie   ");
                        j('.play_caption_filename').html("18.  Building a sales Funnel to Stop Was…ocial Media.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 19){
                        j('.play_caption').html("Lynne Spalding   ");
                        j('.play_caption_filename').html("19.  Turning Profits into Cash.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 20){
                        j('.play_caption').html("Maria Anderson   ");
                        j('.play_caption_filename').html("20.  5 Steps to Creating Content that Sells.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 21){
                        j('.play_caption').html("Melissa Stanford  ");
                        j('.play_caption_filename').html("21.  8 Steps to successfully employ a…r Workplace.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 22){
                        j('.play_caption').html("Neil Daramow  ");
                        j('.play_caption_filename').html("22.  How to write recruitment ads that…ight people.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 23){
                        j('.play_caption').html("Sandra Price  ");
                        j('.play_caption_filename').html("23.  From Shoebox to organised - Gettin…unts sorted.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 24){
                        j('.play_caption').html("Sandra Price  ");
                        j('.play_caption_filename').html("24. The Six Deadly Bookkeeping sins cos…u a fortune.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 25){
                        j('.play_caption').html("Tom Freer  ");
                        j('.play_caption_filename').html("25. Protect Your Business from Cyber Threats.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else if(ID == 26){
                        j('.play_caption').html("Vicki Parker  ");
                        j('.play_caption_filename').html("26. How to run a professional MLM with…our friends.mp3");
                        j('.copy_path').val(_url + "play?id=audio-play-"+ID+"-verse");
                        
                   }else{
                        j('.play_caption').html("No Audio");
                        j('.play_caption_filename').html("No Audio");
                   }
         });


    }
    var scroll = function (){
        jQuery.fn.extend({
        slimScroll: function(o) {

            var ops = o;
            //do it for every element that matches selector
            this.each(function(){

            var isOverPanel, isOverBar, isDragg, queueHide, barHeight,
                divS = '<div></div>',
                minBarHeight = 30,
                wheelStep = 30,
                o = ops || {},
                cwidth = o.width || 'auto',
                cheight = o.height || '250px',
                size = o.size || '7px',
                color = o.color || '#000',
                position = o.position || 'right',
                opacity = o.opacity || .4,
                alwaysVisible = o.alwaysVisible === true;
            
                //used in event handlers and for better minification
                var me = $(this);

                //wrap content
                var wrapper = $(divS).css({
                    position: 'relative',
                    overflow: 'hidden',
                    width: cwidth,
                    height: cheight
                }).attr({ 'class': 'slimScrollDiv' });

                //update style for the div
                me.css({
                    overflow: 'hidden',
                    width: cwidth,
                    height: cheight
                });

                //create scrollbar rail
                var rail  = $(divS).css({
                    width: '15px',
                    height: '100%',
                    position: 'absolute',
                    top: 0
                });

                //create scrollbar
                var bar = $(divS).attr({ 
                    'class': 'slimScrollBar ', 
                    style: 'border-radius: ' + size 
                    }).css({
                        background: color,
                        width: size,
                        position: 'absolute',
                        top: 0,
                        opacity: opacity,
                        display: alwaysVisible ? 'block' : 'none',
                        BorderRadius: size,
                        MozBorderRadius: size,
                        WebkitBorderRadius: size,
                        zIndex: 99
                });

                //set position
                var posCss = (position == 'right') ? { right: '1px' } : { left: '1px' };
                rail.css(posCss);
                bar.css(posCss);

                //wrap it
                me.wrap(wrapper);

                //append to parent div
                me.parent().append(bar);
                me.parent().append(rail);

                //make it draggable
                bar.draggable({ 
                    axis: 'y', 
                    containment: 'parent',
                    start: function() { isDragg = true; },
                    stop: function() { isDragg = false; hideBar(); },
                    drag: function(e) 
                    { 
                        //scroll content
                        scrollContent(0, $(this).position().top, false);
                    }
                });

                //on rail over
                rail.hover(function(){
                    showBar();
                }, function(){
                    hideBar();
                });

                //on bar over
                bar.hover(function(){
                    isOverBar = true;
                }, function(){
                    isOverBar = false;
                });

                //show on parent mouseover
                me.hover(function(){
                    isOverPanel = true;
                    showBar();
                    hideBar();
                }, function(){
                    isOverPanel = false;
                    hideBar();
                });

                var _onWheel = function(e)
                {
                    //use mouse wheel only when mouse is over
                    if (!isOverPanel) { return; }

                    var e = e || window.event;

                    var delta = 0;
                    if (e.wheelDelta) { delta = -e.wheelDelta/120; }
                    if (e.detail) { delta = e.detail / 3; }

                    //scroll content
                    scrollContent(0, delta, true);

                    //stop window scroll
                    if (e.preventDefault) { e.preventDefault(); }
                    e.returnValue = false;
                }

                var scrollContent = function(x, y, isWheel)
                {
                    var delta = y;

                    if (isWheel)
                    {
                        //move bar with mouse wheel
                        delta = bar.position().top + y * wheelStep;

                        //move bar, make sure it doesn't go out
                        delta = Math.max(delta, 0);
                        var maxTop = me.outerHeight() - bar.outerHeight();
                        delta = Math.min(delta, maxTop);

                        //scroll the scrollbar
                        bar.css({ top: delta + 'px' });
                    }

                    //calculate actual scroll amount
                    percentScroll = parseInt(bar.position().top) / (me.outerHeight() - bar.outerHeight());
                    delta = percentScroll * (me[0].scrollHeight - me.outerHeight());

                    //scroll content
                    me.scrollTop(delta);

                    //ensure bar is visible
                    showBar();
                }

                var attachWheel = function()
                {
                    if (window.addEventListener)
                    {
                        this.addEventListener('DOMMouseScroll', _onWheel, false );
                        this.addEventListener('mousewheel', _onWheel, false );
                    } 
                    else
                    {
                        document.attachEvent("onmousewheel", _onWheel)
                    }
                }

                //attach scroll events
                attachWheel();

                var getBarHeight = function()
                {
                    //calculate scrollbar height and make sure it is not too small
                    barHeight = Math.max((me.outerHeight() / me[0].scrollHeight) * me.outerHeight(), minBarHeight);
                    bar.css({ height: barHeight + 'px' });
                }

                //set up initial height
                getBarHeight();

                var showBar = function()
                {
                    //recalculate bar height
                    getBarHeight();
                    clearTimeout(queueHide);
                    
                    //show only when required
                    if(barHeight >= me.outerHeight()) {
                        return;
                    }
                    bar.fadeIn('fast');
                }

                var hideBar = function()
                {
                    //only hide when options allow it
                    if (!alwaysVisible)
                    {
                        queueHide = setTimeout(function(){
                            if (!isOverBar && !isDragg) { bar.fadeOut('slow'); }
                        }, 1000);
                    }
                }

            });
            
            //maintain chainability
            return this;
        }
    });

    jQuery.fn.extend({
        slimscroll: jQuery.fn.slimScroll
    });
    };
     var fileattached = function () {
        j('.fileupload').change(function () {
            var _val = '';
            var _this = j(this);
            var files = _this.prop("files");
            for (var i = 0; i < files.length; i++) {
                _val += (files[i].name) + ',';
            }
            _val = _val.slice(0, -1);
            j('.fake-path').val(_val);
        });
    };

     var add_category = function () {
        var _form = j('.form-category');

        _form.validate({
            rules: {
                categoryname: 'required'
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.errorList, function (index, value) {
                    var _elm = j(value.element);
                    _elm.parent().addClass('has-error');
                    _elm.popover({content: value.message, trigger: 'focus', placement: 'top'});
                });
                j.each(this.successList, function (index, value) {
                    var _elm = j(value);
                    _elm.parent().removeClass('has-error');
                    _elm.popover('destroy');
                });
            },
            submitHandler: function (form) {
                var _form = j(form);
                j.ajax({
                    url: _url + 'postdata',
                    dataType: 'json',
                    method: 'post',
                    data: _form.serialize(),
                    success: function (output) {
                        console.log(_form.serialize());
                        if (output.message === 'success') {
                             j(".tableappend").append("<tr class='"+output.lastid+"'><td width='33.35%' >"+output.lastid+"</td><td width='33.35%'><input type='text' class='category_field form-control' value='"+output.catname+"' name='category_name'></td><td width='33.35%'><input type='hidden' name='id_cat' value='"+output.lastid+"'><button type='submit' cat-id='"+output.lastid+"' class='btn btn-default btn-xs categorytd_update_btn'><i class='fas fa-pen-square'></i>Update</button><button type='submit' cat-del-id='"+output.lastid+"' class='btn btn-danger btn-xs categorytd_delete_btn'><i class='fas fa-trash'></i>Delete</button></td></tr>");
                             console.log(output);
                            j.notify(output);
                            console.log(_form.serialize());
                        

                            
                        }
                        if (output.message === 'exist') {
                             console.log(output);
                            j.notify(output);
                        }
                        /*
                         setTimeout(function () {
                                location.reload();
                            }, 1000);
                         if (output.message === 'success') {
                         j.notify(output);
                         console.log('aal');
                         setTimeout(function () {
                         location.reload();
                         }, 4000);
                         }
                         console.log(output);
                         //_form.find('.output').html(output.text);
                         */
                    }
                });
                return false;
            }
        });
    };
    return {
        init: init
    };
})(jQuery);

