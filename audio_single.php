<?php
include_once ("library.php");
$func = new Library();
$ID_GEN = $_GET['id'];

$exp_value = explode("-", $ID_GEN);
$getreal_id = $exp_value[2];
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http")."://".$_SERVER['SERVER_NAME'];



if($getreal_id == 1){

    $caption_speaker = "Allan Mason";
    $caption_title = "Five steps to determine the right structure for your business.mp3";
}elseif($getreal_id == 2){

    $caption_speaker = "Amanda Dyason";
    $caption_title = "7 Key Numbers You Need to Inc…Your Profits.mp3";
}elseif($getreal_id == 3){

    $caption_speaker = "Amber Worth";
    $caption_title = " Syncing Your Laptop, Phone and Tab…g Office 365.mp3";
}elseif($getreal_id == 4){

    $caption_speaker = "Andrew Cominos";
    $caption_title = "  The 5 Steps to Successful Sales.mp3";
}elseif($getreal_id == 5){

    $caption_speaker = "Andy Tunks";
    $caption_title = "  Creating Engaging Facebook advertise…ving money.mp3";
}elseif($getreal_id == 6){

    $caption_speaker = "Andy Tunks";
    $caption_title = "  Using Facebook advertising to generate saless.mp3";
}elseif($getreal_id == 7){

    $caption_speaker = "Dave Meikle";
    $caption_title = "  5 Strategies to Use Video in Your Sal…ngagement.mp3";
}elseif($getreal_id == 8){

    $caption_speaker = "Dave Meikle";
    $caption_title = "  How to avoid getting overlooked on social media.mp3";
}elseif($getreal_id == 9){

    $caption_speaker = "David Pitman";
    $caption_title = "  Seven Steps to scalable Integrated …anagement.mp3";
}elseif($getreal_id == 10){

    $caption_speaker = "David Pitman";
    $caption_title = "  Choosing the Right CRM for Your Business.mp3";
}elseif($getreal_id == 11){
    $caption_speaker = "Debbie Kropp";
    $caption_title = "  5 Steps to Get Your Website Working for You.mp3";
}elseif($getreal_id == 12){
    $caption_speaker = "Justin Dunkley";
    $caption_title = "  The First Things to do to Kickstart …r Marketing.mp3";
}elseif($getreal_id == 13){
    $caption_speaker = "Kevin Gammie ";
    $caption_title = "  4 Ways We Procrastinate and how to stop it.mp3";
}elseif($getreal_id == 14){
    $caption_speaker = "Kevin Gammie ";
    $caption_title = "  Finding Yor Business Confidence.mp3";
}elseif($getreal_id == 15){
    $caption_speaker = "Kevin Gammie ";
    $caption_title = "   How to build a Workshop.mp3";
}elseif($getreal_id == 16){
    $caption_speaker = "Kevin Gammie ";
    $caption_title = "   The Marketing Plan.mp3";
}elseif($getreal_id == 17){
    $caption_speaker = "Kevin Gammie ";
    $caption_title = "  The Vision.mp3";
}elseif($getreal_id == 18){
    $caption_speaker = "Kevin Gammie ";
    $caption_title = "  Building a sales Funnel to Stop Was…ocial Media.mp3";
}elseif($getreal_id == 19){
    $caption_speaker = "Lynne Spalding ";
    $caption_title = "  Turning Profits into Cash.mp3";
}elseif($getreal_id == 20){
    $caption_speaker = " Maria Anderson  ";
    $caption_title = "  5 Steps to Creating Content that Sells.mp3";
}elseif($getreal_id == 21){
    $caption_speaker = "  Melissa Stanford  ";
    $caption_title = " 8 Steps to successfully employ a…r Workplace.mp3";
}elseif($getreal_id == 22){
    $caption_speaker = " Neil Daramow  ";
    $caption_title = " How to write recruitment ads that…ight people.mp3";
}elseif($getreal_id == 23){
    $caption_speaker = " Sandra Price ";
    $caption_title = "  From Shoebox to organised - Gettin…unts sorted.mp3";
}elseif($getreal_id == 24){
    $caption_speaker = " Sandra Price ";
    $caption_title = "   The Six Deadly Bookkeeping sins cos…u a fortune.mp3";
}elseif($getreal_id == 25){
    $caption_speaker = " Tom Freer ";
    $caption_title = "  Protect Your Business from Cyber Threats.mp3";
}elseif($getreal_id == 26){
    $caption_speaker = " Vicki Parker ";
    $caption_title = "  How to run a professional MLM with…our friends.mp3";
}else{
     $caption_speaker = "";
}

?>

    <div class="container" style="padding-top:35px;">
         <div class="row" style="background-color: #446CB3;">
            <div class="col-sm-12" style="padding:12px;">
                <img src="images/logo-light.svg" height="80px;">
                <br>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-3">
                

            </div>
            <div class="col-sm-7">

                <div class="row">
                    <div class="col-sm-12 ">

                        <div class="panel panel-default audio-banner" style="text-align:center;padding:15px">
                            <img src="images/mic.png" class="img-profile" style="height:380px;">
                            <h1 class="play_caption"><?php echo  $caption_speaker; ?></h1>
                            <h5 class="play_caption_filename"><?php echo $caption_title; ?></h5>
                            <audio id="blast" class="filepath" src="<?php echo $actual_link."/audio/uploads/".$getreal_id.".mp3"; ?>" controls "loop" >

                            </audio>
                        </div>
                    </div>
                </div>
              

             
            </div>
            <div class="col-sm-2">
                
            </div>

        </div>
       
        <div class="modal fade" id="category" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Category</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                        <form class="form-category" method="post" enctype="multipart/form-data">
                            <div class="col-sm-3">
                                <strong>Category Name</strong>
                            </div>
                            <div class="col-sm-7" style="text-align: left;">
                                <input type="text" class="form-control categoryname" name="categoryname" placeholder="Enter category name" style="border: 1px solid gray;">
                            </div>
                            <div class="col-sm-2" style="padding-bottom: 10px;">
                                    
                            </div>
                            <div class="col-sm-3">
                                <strong>File Upload</strong>
                            </div>
                            <div class="col-sm-7" style="text-align: left;">
                               <div class="input-group"> 
                             
                               <input type="text" class="form-control fake-path" placeholder="doc,ppt,xsl,pdf,image only" readonly="readonly" aria-invalid="false"> <span class="input-group-btn"> <button class="btn btn-default attached" type="button">Attach file <i class="fa fa-paperclip" aria-hidden="true"></i> MAX 5MB</button> 
                           </div>
                            </div>
                            <div class="col-sm-2" style="padding-bottom: 10px;">
                                   <input type="file" name="file[]" class="form-control fileupload" multiple="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword, application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*">
                                     
                            </div>

                            
                             <div class="col-sm-2">
                                <button type="submit" class="btn btn-success addcategory">Add</button>
                            </div>
                        </form>
                        
                        <form class="form-category-search" method="post">
                            <div class="col-sm-3" style="padding: 10px;">
                                <strong>Search Category</strong>
                            </div>
                            <div class="col-sm-7" style="text-align: left;">
                                <input type="text" class="form-control filtercategory" name="filtercategory" placeholder="Type category name" style="border: 1px solid gray;">
                            </div>
                            <div class="col-sm-2">
                               
                            </div>
                        </form>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    
                                    <table class="table" width="100%">
                                        <thead >
                                            <tr>
                                                <th>#ID</th>
                                                <th>Category Name</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody  class="searchablecategory" >
                                        <?php
                                           // $sql = "SELECT * FROM category_tbl";
                                           // $result = $conn->query($sql);
                                           // while($row = $result->fetch_assoc()) {
                                        ?>
                                        <form  class="form_info" >
                                            <tr class= "<?php echo  $row["ID"]; ?>">
                                                <td width="33.35%" ><?php echo  $row["ID"]; ?></td>
                                                <td width="33.35%"><input type="text" class="category_field form-control" value="<?php echo  $row["category_name"]; ?>" name="category_name"></td>
                                                <td width="33.35%">
                                                    <input type="hidden" name="id_cat" value="<?php echo  $row["ID"]; ?>">
                                                     <button type="submit" cat-id="<?php echo  $row["ID"]; ?>" class="btn btn-default btn-xs categorytd_update_btn"><i class="fas fa-pen-square"></i>Update</button>

                                                      <button type="submit" cat-del-id="<?php echo  $row["ID"]; ?>" class="btn btn-danger btn-xs categorytd_delete_btn"><i class="fas fa-trash"></i>Delete</button>
                                                  
                                                </td>

                                            </tr>
                                         </form>
                                        <?php
                                          //  }
                                        ?>
                                        
                                        </tbody>
                                    </table>
                                    <table class="table" width="100%">
                                        <tbody class="tableappend searchablecategory">
                                          <form class="tableappend form_info_append" novalidate="novalidate">

                                          </form>
                                        </tbody>
                                    </table>
                               
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {

        });
    </script>