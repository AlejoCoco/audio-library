<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "soundcloud_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

class Library {

    function base_url(){
         return sprintf(
    "%s://%s%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME'],
    $_SERVER['REQUEST_URI']
        );
    }
}

?>